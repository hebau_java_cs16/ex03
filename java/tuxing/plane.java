package ex03;

public abstract class plane {   //定义平面抽象类
	int a, b, r;
	double l, s;
	static final double PI= 3.14;

	public double Circularl(int r) {   //求圆面积
		l = PI * r * r;
		return l;
	}

	public double Circulars(int r) {  //求圆周长
		s = 2 * PI * r;
		return s;
	}

	public int Squares(int a, int b) {   //矩形周长
		return (a + b) * 2;
	}

	public int Squarev(int a, int b) {   //矩形面积
		return a * b;
	}

	public int Trianglel(int a, int b, int c) {  //三角形周长
		return a + b + c;
	}

	public double Triangles(int a, int b, int c) {   //三角形面积
		double p = (a + b + c) / 2;
		double n = p * (p - a) * (p - b) * (p - c);
		s = Math.sqrt(n);
		return s;
	}

}
