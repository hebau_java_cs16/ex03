package ex03;

public class cone extends solid {   //圆锥类
	private int r;
	private int l;
	private int h;
	public cone(int r,int l,int h){
		this.r=r;
		this.l=l;
		this.h=h;
	}
	public int getr() {
		return r;
	}
	public void setr(int r) {
		this.r = r;
	}
	public int getl() {
		return l;
	}
	public void setl(int l) {
		this.l = l;
	}
	public int geth() {
		return h;
	}
	public void seth(int h) {
		this.h = h;
	}
	/*public String toString(){
		return "圆锥的表面积："+super.conicals(getr(),getl())+" 圆锥的体积："+conicalv(getr(),geth());
	}*/
}
