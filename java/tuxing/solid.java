package ex03;

//import java.math.BigDecimal;

public abstract class solid {   // 定义立体抽象类
	public static final double PI= 3.14;
	static double s;   
	static double v;

	public static double spheres(int r) {   //球的面积
		s = 4.0 * PI* r * r;
		return s;
	}

	public static double spherev(int r) {    //球的体积
		v = (4.0 / 3.0) * PI * r * r * r;
		return v;
	}

	public static double cylinders(int r, int h) {   //圆柱面积
		s = 2.0 *PI * r * r + PI * r * 2.0 * h;
		return s;
	}

	public static double cylinderv(int r, int h) {   //圆柱体积
		v = PI * r * r * h;
		return v;
	}

	public static double conicals(int r, int l) {   //圆锥表面积
		s = PI * r * l + PI * r * r;
		return s;
	}

	public static double conicalv(int r, int h) {   //圆锥体积
		v = (1.0 / 3.0) * PI* r * r * h;
		return v;
	}
}
