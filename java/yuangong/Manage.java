package yuangong;

public class Manage extends Work{
	private String post;  //职务属性
	private int money; //年薪属性
	public Manage(){}     //定义无参构造方法
	public Manage(String name, int age, String sex,String post, int money) {
		super(name,age,sex);
		this.post = post;
		this.money =money;
	}
	public String getpost() {  //设置职务
		return post;
	}
	public void setpost(String post) {  //取得职务
		this.post = post;
	}
	public int getmoney() {  //设置年薪
		return money;
	}
	public void setmMoney(int mMoney) {  //取得年薪
		this.money= money;
	}
	public String toString(){
		return this.getname()+this.getage()+this.getsex()+this.getpost()+this.getmoney();
	}
}
