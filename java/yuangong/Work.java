package yuangong;

public class Work {
	private String name;
	private int age;
	private String sex;
	public Work() {}         //定义无参构造方法
	public Work(String name,int age,String sex) {
		this.name=name;
		this.age=age;
		this.sex=sex;
	}
	public String getname() {//设置姓名
		return name;
	}
	public void setname(String name) {//取得姓名
		this.name = name;
	}
	public String getsex() {//设置性别
		return sex;
	}
	public void setsex(String sex) {//取得性别
		this.sex = sex;
	}
	public int getage() {//设置年龄
		return age;
	}
	public void setage(int age) {//取得年龄
		this.age = age;
	}
}
