package animal2;

public class Dog implements Animal {
	private String name;

	public Dog(String name) {}

	public void setName(String name) { // 取得宠物名字
		this.name = name;
	}

	public String getName() { // 设置宠物名字
		return this.name;
	}

	@Override
	public void cry() {
		System.out.println("小狗 汪汪叫！");

	}

	@Override
	public void getAnimaName() {
		System.out.println("汪汪叫的是小狗！");
		System.out.print("小狗的名字是：" + this.getName());
	}

}
