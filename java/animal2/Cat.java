package animal2;

public class Cat implements Animal {
	private String name;

	public Cat(String name) {}
	public void setName(String name) { // 取得宠物名字
		this.name = name;
	}

	public String getName() { // 设置宠物名字
		return this.name;
	}

	@Override
	public void cry() {
		System.out.println("小猫 喵喵叫！");

	}

	@Override
	public void getAnimaName() {
		System.out.println("喵喵叫的是小猫！");
		System.out.print("小猫的名字是："+this.getName());
	}

}
