package yinhang;
import java.util.Scanner;
public class Bank {
	private String name;
	private String code;
	private double money;
	public Bank(){}
	public Bank(String name,String code,double money){
		this.name=name;
		this.code=code;
		this.money=money;
	}
	public String getname(){  //设置姓名内容
		return name;
	}
	public String getcode(){  //设置密码内容
		return code;
	}
	public double getmoney(){  //设置金额内容
		return money;
	}
	public void setname(String name){  //取得姓名内容
		this.name=name;
	}
	public void setcode(String code){   //取得密码内容
		this.code=code;
	}
	public void setmoney(double money){    //取得金额内容
		this.money=money;
	}
	public static void welcome() {
		System.out.println("欢迎来到建设银行");
	}
	public double kaihu() {
		Scanner word=new Scanner(System.in);
		System.out.println("请输入用户姓名：");
		String name=word.next();
		System.out.println("请设置账户密码：");
		String code=word.next();
		System.out.println("请输入开户金额：");
		double money=word.nextDouble();
		money=money-10;
		System.out.printf(name+"开户成功，账户余额"+money);
		return money;
	}
	public void deposit(double money){
		Scanner word=new Scanner(System.in);
		System.out.println("请输入存款金额：");
		double money1=word.nextDouble();
		money=money+money1;
		System.out.printf(name+"您好，您的账户已存入"+money1+",当前余额"+money);
	}
	public void withdrawal(double money,String code) {
		Scanner word=new Scanner(System.in);
		System.out.println("请输入取款密码：");
		String code1=word.next();
		System.out.println("请输入取款金额：");
		double money1=word.nextDouble();
		if(code.equals(code1)){
			this.money -= money1;
			System.out.printf(name+"您好，您的账户已取出"+money1+",当前余额"+money);
        }
		else {
			System.out.println("输入密码错误！！！");
		}
		if(money <= 0){
            System.out.println("取款金额必须大于0");
        }
        if(this.money <= money1){
            System.out.println("余额不足，无法取款");
        }
	}
	public static void welcomeNext() {
		System.out.println("请携带好随身物品，欢迎下次光临");
	}
}
